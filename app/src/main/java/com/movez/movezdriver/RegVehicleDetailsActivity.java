package com.movez.movezdriver;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

public class RegVehicleDetailsActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_reg_vehicle_details);
    }

    public void goBack(View view){
        Intent intent = new Intent(this, RegCarCategoryActivity.class);
        startActivity(intent);
    }

    public void clickSubmit(View view){
        Intent intent = new Intent(this, RegDriverDocsActivity.class);
        startActivity(intent);
    }
}
