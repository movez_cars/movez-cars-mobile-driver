package com.movez.movezdriver;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

public class HomeActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
    }

    public void logout(View view){
        Intent intent = new Intent(this, LoginActivity.class);
        startActivity(intent);
    }

    public void goOnline(View view){
        Intent intent = new Intent(this, OnlineActivity.class);
        startActivity(intent);
    }

    public void changeVehicle(View view){
        Intent intent = new Intent(this, ChangeVehicleActivity.class);
        startActivity(intent);
    }

    public void changeCategory(View view){
        Intent intent = new Intent(this, ChangeCategoryActivity.class);
        startActivity(intent);
    }
}
