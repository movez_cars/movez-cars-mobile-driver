package com.movez.movezdriver;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

public class RegDriverDocsActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_reg_driver_docs);
    }

    public void goBack(View view){
        Intent intent = new Intent(this, RegVehicleDetailsActivity.class);
        startActivity(intent);
    }

    public void clickSubmit(View view){
        Intent intent = new Intent(this, HomeActivity.class);
        startActivity(intent);
    }
}
